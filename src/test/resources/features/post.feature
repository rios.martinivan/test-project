Feature: Posts can be created, updated, deleted, and fully queried
  You should be able to create, update, or delete any posts, and
  all and any specific posts should be able to be queried, including its comments and author

  Scenario: Get all the posts
    Given someone has a query to get all posts
    When someone sends the query
    Then the service returns multiple posts
    And the posts returned have an author and comments

  Scenario Template: Get a post by id
    Given someone has a query to get a post by the id "<id>"
    When someone sends the query
    Then the service returns a single posts
    And the posts returned have an author and comments

    Examples:
      | id |
      |  1 |
      |  3 |
      |  5 |

  Scenario: Get a non-existent post by id
    Given someone has a query to get a post by the id "987654321"
    When someone sends the query
    Then the service returns an error with the message "Post not found with id: 987654321"

  Scenario: Create a post
    Given someone has a mutation to create a post
    When someone sends the query
    Then the service returns the post id

  Scenario: Update a post by id
    Given someone has a mutation to update a post
    When someone sends the query
    Then the service returns the post id

  Scenario: Update a non-existent post by id
    Given someone has a mutation to update a post by the id "987654321"
    When someone sends the query
    Then the service returns an error with the message "Post not found with id: 987654321"

  Scenario: Delete a post by id
    Given someone has a mutation to delete a post
    When someone sends the query
    Then the service returns the post id