package group.phorus.testservice.bdd.steps

import com.fasterxml.jackson.databind.ObjectMapper
import group.phorus.testservice.bdd.RequestScenarioScope
import group.phorus.testservice.bdd.ResponseScenarioScope
import group.phorus.testservice.model.dbentities.Post
import group.phorus.testservice.repositories.PostRepository
import group.phorus.testservice.repositories.UserRepository
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import kotlin.test.assertNotNull
import kotlin.test.assertTrue
import kotlin.test.fail

internal class PostStepsDefinition(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val postRepository: PostRepository,
    @Autowired private val requestScenarioScope: RequestScenarioScope,
    @Autowired private val responseScenarioScope: ResponseScenarioScope,
    @Autowired private val objectMapper: ObjectMapper,
) {
    @Given("someone has a mutation to create a post")
    fun `someone has a mutation to create a post`() {
        requestScenarioScope.apply {
            query = """
                mutation {
                  createPost(post: {
                    title: "testTitle",
                    content: "testContent",
                  }, authorId: "1")
                }
            """.trimIndent()
            queryType = "createPost"
        }
    }

    @Given("someone has a mutation to update a post{byTheId}")
    fun `someone has a mutation to update a post{byTheId}`(byTheId: String?) {
        requestScenarioScope.apply {
            query = """
                mutation {
                  updatePost(id: "${byTheId ?: "1"}", post: {
                    title: "testTitle",
                    content: "testContent",
                  })
                }
            """.trimIndent()
            queryType = "updatePost"
        }
    }

    @Given("someone has a mutation to delete a post")
    fun `someone has a mutation to delete a post{byTheId}`() {
        val id = postRepository.save(Post(
            title = "test",
            content = "test",
            author = userRepository.findAll().first(),
        )).id!!

        requestScenarioScope.apply {
            query = """
                mutation {
                  deletePost(id: "$id")
                }
            """.trimIndent()
            queryType = "deletePost"
        }
    }


    @Then("the posts returned have an author and comments")
    fun `the posts returned have an author and comments`() {
        val response = with (requestScenarioScope.queryType!!) { when {
            endsWith("s") -> {
                val first = (responseScenarioScope.response as List<*>).first()
                objectMapper.convertValue(first, Post::class.java)
            }
            endsWith("ById") -> objectMapper.convertValue(responseScenarioScope.response, Post::class.java)
            else -> fail(("Unrecognized query type"))
        }}

        assertTrue(response.comments.isNotEmpty())
        assertNotNull(response.author)
    }
}