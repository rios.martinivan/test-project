package group.phorus.testservice.bdd.steps

import com.fasterxml.jackson.databind.ObjectMapper
import group.phorus.testservice.bdd.RequestScenarioScope
import group.phorus.testservice.bdd.ResponseScenarioScope
import io.cucumber.java.ParameterType
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.graphql.ResponseError
import org.springframework.graphql.test.tester.GraphQlTester
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue
import kotlin.test.fail

@ParameterType("(\\S*)")
fun queryType(queryType: String) = queryType.lowercase()

@ParameterType("(by id|by the id:? \"\\S*\")")
fun byId(byId: String): String = with (byId) { when {
    contains("by the id") -> byId.split("\\s".toRegex()).last().removeSurrounding("\"")
    else -> ""
}}

@ParameterType("( by the id \"\\S*\")?")
fun byTheId(byTheId: String?): String? =
    byTheId?.split("\\s".toRegex())?.last()?.removeSurrounding("\"")

internal class BaseStepsDefinition(
    @Autowired private val graphQlTester: GraphQlTester,
    @Autowired private val requestScenarioScope: RequestScenarioScope,
    @Autowired private val responseScenarioScope: ResponseScenarioScope,
    @Autowired private val objectMapper: ObjectMapper,
) {
    @Given("someone has a query to get a {queryType} {byId}")
    fun `someone has a query to get a {queryType} {byId}`(type: String, byId: String) {
        requestScenarioScope.apply {
            query = """
                query {
                  ${type.dropLastWhile { it == 's' }}ById(id: "${byId.ifEmpty { "1" }}") {
                    id
                    ${getFields(type)}
                  }
                }
            """.trimIndent()
            queryType = "${type.dropLastWhile { it == 's' }}ById"
        }
    }

    @Given("someone has a query to get all {queryType}")
    fun `someone has a query to get all {entityType}`(type: String) {
        if (type.contains("comment")) fail("Not implemented")

        requestScenarioScope.apply {
            query = """
                query {
                  $type {
                    ${getFields(type)}
                  }
                }
            """.trimIndent()
            queryType = "${type.dropLastWhile { it == 's' }}s"
        }
    }


    @When("someone sends the query")
    fun `someone sends a query`() {
        val entity = with (requestScenarioScope.queryType!!) { when {
            endsWith("s") -> object: ParameterizedTypeReference<List<LinkedHashMap<*, *>>>() {}
            endsWith("ById") -> object: ParameterizedTypeReference<LinkedHashMap<*, *>>() {}
            else -> object: ParameterizedTypeReference<String>() {}
        }}

        val response = runCatching {
            graphQlTester.document(requestScenarioScope.query!!)
                .execute()
                .path("data.${requestScenarioScope.queryType!!}")
                .entity(entity)
                .get()
        }.getOrElse {
            var errors: List<ResponseError> = emptyList()

            graphQlTester.document(requestScenarioScope.query!!)
                .execute()
                .errors()
                .filter { true }
                .satisfy { errors = it }

            errors
        }

        responseScenarioScope.response = response
    }


    @Then("^the service returns a single \\S*$")
    fun `the service returns a single S`() {
        assertNotNull(responseScenarioScope.response)
        assertThrows<IllegalArgumentException> {
            objectMapper.convertValue(responseScenarioScope.response, List::class.java)
        }
    }

    @Then("^the service returns multiple \\S*$")
    fun `the service returns multiple S`() {
        val response = responseScenarioScope.response as List<*>
        assertTrue(response.isNotEmpty())
    }

    @Then("^the service returns the \\S* id$")
    fun `the service returns the S id`() {
        val response = responseScenarioScope.response as String
        assertTrue(response.isNotBlank())
    }

    @Then("the service returns an error with the message {string}")
    fun `the service returns an error with the message {string}`(errorMessage: String) {
        @Suppress("UNCHECKED_CAST")
        val errors = responseScenarioScope.response as List<ResponseError>

        assertEquals(errorMessage, errors.first().message)
    }

    private fun getFields(type: String) = with (type) { when {
        contains("user") -> """
            id
            posts { id }
            comments { id }
        """
        contains("post") -> """
            id
            author { id }
            comments { id }
        """
        contains("comment") -> """
            id
            post { id }
            author { id }
        """
        else -> """
           id 
        """
    }}
}