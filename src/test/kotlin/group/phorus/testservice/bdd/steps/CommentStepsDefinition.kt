package group.phorus.testservice.bdd.steps

import com.fasterxml.jackson.databind.ObjectMapper
import group.phorus.testservice.bdd.RequestScenarioScope
import group.phorus.testservice.bdd.ResponseScenarioScope
import group.phorus.testservice.model.dbentities.Comment
import group.phorus.testservice.repositories.CommentRepository
import group.phorus.testservice.repositories.PostRepository
import group.phorus.testservice.repositories.UserRepository
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import kotlin.test.assertNotNull
import kotlin.test.fail

internal class CommentStepsDefinition(
    @Autowired private val commentRepository: CommentRepository,
    @Autowired private val userRepository: UserRepository,
    @Autowired private val postRepository: PostRepository,
    @Autowired private val requestScenarioScope: RequestScenarioScope,
    @Autowired private val responseScenarioScope: ResponseScenarioScope,
    @Autowired private val objectMapper: ObjectMapper,
) {
    @Given("someone has a mutation to create a comment")
    fun `someone has a mutation to create a comment`() {
        requestScenarioScope.apply {
            query = """
                mutation {
                  createComment(comment: {
                    content: "testContent",
                  }, authorId: "1", postId: "1")
                }
            """.trimIndent()
            queryType = "createComment"
        }
    }

    @Given("someone has a mutation to update a comment{byTheId}")
    fun `someone has a mutation to update a comment{byTheId}`(byTheId: String?) {
        requestScenarioScope.apply {
            query = """
                mutation {
                  updateComment(id: "${byTheId ?: "1"}", comment: {
                    content: "testContent",
                  })
                }
            """.trimIndent()
            queryType = "updateComment"
        }
    }

    @Given("someone has a mutation to delete a comment")
    fun `someone has a mutation to delete a comment{byTheId}`() {
        val id = commentRepository.save(
            Comment(
                content = "test",
                author = userRepository.findAll().first(),
                post = postRepository.findAll().first(),
            )
        ).id!!

        requestScenarioScope.apply {
            query = """
                mutation {
                  deleteComment(id: "$id")
                }
            """.trimIndent()
            queryType = "deleteComment"
        }
    }


    @Then("the comments returned have an author and a post")
    fun `the comments returned have an author and a post`() {
        val response = with (requestScenarioScope.queryType!!) { when {
            endsWith("s") -> {
                val first = (responseScenarioScope.response as List<*>).first()
                objectMapper.convertValue(first, Comment::class.java)
            }
            endsWith("ById") -> objectMapper.convertValue(responseScenarioScope.response, Comment::class.java)
            else -> fail(("Unrecognized query type"))
        }}

        assertNotNull(response.author)
        assertNotNull(response.post)
    }
}