package group.phorus.testservice.bdd

import io.cucumber.spring.CucumberContextConfiguration
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@CucumberContextConfiguration
@ActiveProfiles("test")
@AutoConfigureGraphQlTester
internal class CucumberSpringConfiguration