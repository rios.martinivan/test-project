package group.phorus.testservice.services.impl

import group.phorus.testservice.model.dbentities.Post
import group.phorus.testservice.model.dtos.PostDTO
import group.phorus.testservice.repositories.PostRepository
import group.phorus.testservice.repositories.UserRepository
import group.phorus.testservice.services.PostService
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.Pageable
import org.springframework.test.context.ActiveProfiles
import java.util.*
import kotlin.jvm.optionals.getOrNull
import kotlin.test.*


@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PostServiceImplTest(
    @Autowired private val postRepository: PostRepository,
    @Autowired private val postService: PostService,
    @Autowired private val userRepository: UserRepository,
) {

    @Test
    fun `find all posts`(): Unit = runBlocking {
        val response = postService.findAll(Pageable.unpaged())

        assertTrue(response.size > 0)
    }

    @Test
    fun `find post by id`(): Unit = runBlocking {
        val id = postRepository.findAll().first().id!!

        val response = postService.findById(id)

        assertEquals(id, response.id)
    }

    @Test
    fun `find post by author id`(): Unit = runBlocking {
        val id = userRepository.findAll().first().id!!

        val response = postService.findAllByAuthorId(id, Pageable.unpaged())

        assertTrue(response.size > 0)
    }

    @Test
    fun `findById - post not found`(): Unit = runBlocking {
        val id = UUID.randomUUID().toString()
        try {
            postService.findById(id)
            fail("Exception not thrown")
        } catch (ex: Exception) {
            assertEquals("Post not found with id: $id", ex.message)
        }
    }

    @Test
    fun `create a post`(): Unit = runBlocking {
        val authorId = userRepository.findAll().first().id!!
        val response = postService.create(createPostDto(), authorId)

        val saved = postRepository.findById(response).getOrNull()
        assertNotNull(saved)
    }

    @Test
    fun `update a post`(): Unit = runBlocking {
        val post = postRepository.findAll().first()

        postService.update(post.id!!, PostDTO(title = "test2", content = "testContent2"))

        val updated = postRepository.findById(post.id!!).get()

        assertEquals("test2", updated.title)
        assertEquals("testContent2", updated.content)
    }

    @Test
    fun `update - only title`(): Unit = runBlocking {
        val post = postRepository.findAll().first()

        postService.update(post.id!!, PostDTO(title = "test2"))

        val updated = postRepository.findById(post.id!!).get()

        assertEquals("test2", updated.title)
        assertNotNull(updated.content)
    }

    @Test
    fun `update - post not found`(): Unit = runBlocking {
        val id = UUID.randomUUID().toString()

        try {
            postService.update(id, createPostDto())
            fail("Exception not thrown")
        } catch (ex: Exception) {
            assertEquals("Post not found with id: $id", ex.message)
        }
    }

    @Test
    fun `delete a post by id`(): Unit = runBlocking {
        val id = postRepository.save(Post(
            title = "test",
            content = "test",
            author = userRepository.findAll().first(),
        )).id!!

        postService.deleteById(id)

        val deleted = postRepository.findById(id).getOrNull()

        assertNull(deleted)
    }

    private fun createPostDto(): PostDTO =
        PostDTO("testTitle", "testContent")
}