package group.phorus.testservice.services

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface CrudService<A, B, C> {

    suspend fun findAll(pageable: Pageable): Page<A>
    suspend fun findById(id: C): A
    suspend fun create(dto: B): C
    suspend fun update(id: C, dto: B)
    suspend fun deleteById(id: C)
}