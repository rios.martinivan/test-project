package group.phorus.testservice.services.impl;

import group.phorus.testservice.exceptions.ResourceNotFoundException
import group.phorus.testservice.model.dbentities.User
import group.phorus.testservice.model.dtos.UserDTO
import group.phorus.testservice.repositories.UserRepository
import group.phorus.testservice.services.UserService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(
    private val userRepository: UserRepository,
) : UserService {

    override suspend fun findAll(pageable: Pageable) =
        withContext(Dispatchers.IO) { userRepository.findAll(pageable) }

    override suspend fun findById(id: String): User =
        withContext(Dispatchers.IO) {
            userRepository.findById(id).orElseThrow {
                ResourceNotFoundException("User not found with id: $id")
            }
        }

    override suspend fun create(dto: UserDTO) =
        User(
            name = dto.name,
            email = dto.email,
        ).let {
            withContext(Dispatchers.IO) { userRepository.save(it) }
        }.id!!

    override suspend fun update(id: String, dto: UserDTO) {
        val user = findById(id)

        user.apply {
            dto.name?.let { name = it }
            dto.email?.let { email = it }
        }

        withContext(Dispatchers.IO) { userRepository.save(user) }
    }

    override suspend fun deleteById(id: String): Unit =
        withContext(Dispatchers.IO) { runCatching { userRepository.deleteById(id) }}
}
