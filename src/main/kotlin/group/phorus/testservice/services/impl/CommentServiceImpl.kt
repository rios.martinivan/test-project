package group.phorus.testservice.services.impl;

import group.phorus.testservice.exceptions.ResourceNotFoundException
import group.phorus.testservice.model.dbentities.Comment
import group.phorus.testservice.model.dtos.CommentDTO
import group.phorus.testservice.repositories.CommentRepository
import group.phorus.testservice.services.CommentService
import group.phorus.testservice.services.PostService
import group.phorus.testservice.services.UserService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class CommentServiceImpl(
    private val commentRepository: CommentRepository,
    private val userService: UserService,
    private val postService: PostService,
) : CommentService {

    override suspend fun findAll(pageable: Pageable) =
        withContext(Dispatchers.IO) { commentRepository.findAll(pageable) }

    override suspend fun findById(id: String): Comment =
        withContext(Dispatchers.IO) {
            commentRepository.findById(id).orElseThrow {
                ResourceNotFoundException("Comment not found with id: $id")
            }
        }

    override suspend fun findAllByAuthorId(userId: String, pageable: Pageable) =
        withContext(Dispatchers.IO) {
            commentRepository.findAllByAuthorId(userId, pageable)
        }

    override suspend fun findAllByPostId(postId: String, pageable: Pageable) =
        withContext(Dispatchers.IO) {
            commentRepository.findAllByPostId(postId, pageable)
        }

    override suspend fun create(dto: CommentDTO): String =
        throw ResourceNotFoundException("Not implemented")

    override suspend fun create(dto: CommentDTO, authorId: String, postId: String) =
        Comment(
            content = dto.content,
            author = userService.findById(authorId),
            post = postService.findById(postId),
        ).let {
            withContext(Dispatchers.IO) { commentRepository.save(it) }
        }.id!!

    override suspend fun update(id: String, dto: CommentDTO) {
        val comment = commentRepository.findById(id).orElseThrow {
            ResourceNotFoundException("Comment not found with id: $id")
        }

        comment.apply {
            dto.content?.let { content = it }
        }

        withContext(Dispatchers.IO) { commentRepository.save(comment) }
    }

    override suspend fun deleteById(id: String): Unit =
        withContext(Dispatchers.IO) { runCatching { commentRepository.deleteById(id) }}
}
