package group.phorus.testservice.services.impl;

import group.phorus.testservice.exceptions.ResourceNotFoundException
import group.phorus.testservice.model.dbentities.Post
import group.phorus.testservice.model.dtos.PostDTO
import group.phorus.testservice.repositories.PostRepository
import group.phorus.testservice.services.PostService
import group.phorus.testservice.services.UserService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class PostServiceImpl(
    private val postRepository: PostRepository,
    private val userService: UserService,
) : PostService {

    override suspend fun findAll(pageable: Pageable) =
        withContext(Dispatchers.IO) { postRepository.findAll(pageable) }

    override suspend fun findById(id: String): Post =
        withContext(Dispatchers.IO) {
            postRepository.findById(id).orElseThrow {
                ResourceNotFoundException("Post not found with id: $id")
            }
        }

    override suspend fun findAllByAuthorId(userId: String, pageable: Pageable) =
        withContext(Dispatchers.IO) {
            postRepository.findAllByAuthorId(userId, pageable)
        }

    override suspend fun create(dto: PostDTO): String =
        throw ResourceNotFoundException("Not implemented")

    override suspend fun create(dto: PostDTO, authorId: String) =
        Post(
            title = dto.title,
            content = dto.content,
            author = userService.findById(authorId),
        ).let {
            withContext(Dispatchers.IO) { postRepository.save(it) }
        }.id!!

    override suspend fun update(id: String, dto: PostDTO) {
        val post = postRepository.findById(id).orElseThrow {
            ResourceNotFoundException("Post not found with id: $id")
        }

        post.apply {
            dto.title?.let { title = it }
            dto.content?.let { content = it }
        }

        withContext(Dispatchers.IO) { postRepository.save(post) }
    }

    override suspend fun deleteById(id: String): Unit =
        withContext(Dispatchers.IO) { runCatching { postRepository.deleteById(id) }}
}
