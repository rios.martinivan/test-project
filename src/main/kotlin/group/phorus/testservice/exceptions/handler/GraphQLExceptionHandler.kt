package group.phorus.testservice.exceptions.handler

import graphql.GraphQLError
import graphql.language.SourceLocation
import graphql.schema.DataFetchingEnvironment
import group.phorus.testservice.exceptions.BadRequestException
import group.phorus.testservice.exceptions.ResourceNotFoundException
import jakarta.validation.ConstraintViolationException
import org.springframework.graphql.execution.DataFetcherExceptionResolver
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import java.util.*
import java.util.stream.Collectors

@Component
class GraphQLExceptionHandler : DataFetcherExceptionResolver {
    override fun resolveException(
        exception: Throwable,
        environment: DataFetchingEnvironment
    ): Mono<List<GraphQLError>> {
        val sourceLocation: List<SourceLocation> = listOf(environment.field.sourceLocation)
        return when (exception) {
            is ConstraintViolationException ->
                Mono.just(
                    handleConstraintViolationException(
                        exception,
                        sourceLocation
                    )
                )
            is ResourceNotFoundException ->
                exception.apply {
                    locs = sourceLocation
                }.let { Mono.just(Collections.singletonList(it)) }
            is BadRequestException ->
                exception.apply {
                    locs = sourceLocation
                }.let { Mono.just(Collections.singletonList(it)) }
            else -> Mono.empty()
        }
    }

    private fun handleConstraintViolationException(
        exception: ConstraintViolationException,
        locations: List<SourceLocation>
    ): List<GraphQLError> = exception.constraintViolations.stream()
        .map { constraint -> BadRequestException(constraint.messageTemplate, locations) }
        .collect(Collectors.toList())
}