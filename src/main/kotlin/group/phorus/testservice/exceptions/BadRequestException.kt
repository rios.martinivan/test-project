package group.phorus.testservice.exceptions

import graphql.ErrorClassification
import graphql.language.SourceLocation
import org.springframework.graphql.execution.ErrorType
import org.springframework.http.HttpStatus


class BadRequestException(
    private val msg: String = "Bad request",
    var locs: List<SourceLocation> = emptyList(),
) : BaseException() {

    private val status = HttpStatus.BAD_REQUEST
    override fun errorMessage(): String = msg

    override fun getLocations(): List<SourceLocation> = locs

    override fun getErrorType(): ErrorClassification = ErrorType.BAD_REQUEST

    override fun getExtensions(): Map<String, Any> = mapOf("errorCode" to status.value())
}
