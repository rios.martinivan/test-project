package group.phorus.testservice.repositories

import group.phorus.testservice.model.dbentities.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface UserRepository: JpaRepository<User, String>
