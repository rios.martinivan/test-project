package group.phorus.testservice.repositories

import group.phorus.testservice.model.dbentities.Comment
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface CommentRepository: JpaRepository<Comment, String> {
    fun findAllByAuthorId(authorId: String, pageable: Pageable): Page<Comment>
    fun findAllByPostId(postId: String, pageable: Pageable): Page<Comment>
}
