package group.phorus.testservice.model.dtos

class PostDTO(
    var title: String? = null,
    var content: String? = null,
)