package group.phorus.testservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestServiceApplication

fun main(args: Array<String>) {
	runApplication<group.phorus.testservice.TestServiceApplication>(*args)
}
